python-hbmqtt (0.9.6-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from old 10 to 12.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.
  * Update standards version to 4.5.0, no changes needed.

  [ Ondřej Nový ]
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Stein Magnus Jodal ]
  * Remove myself from Uploaders
  * New upstream release
  * d/control: Set Rules-Requires-Root
  * d/copyright: Update copyright years
  * d/control: Enable autopkgtest
  * d/patches: Refresh patch

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 04 Dec 2020 23:52:25 +0100

python-hbmqtt (0.9.5-3) unstable; urgency=medium

  * d/clean: Clean .pytest_cache/
  * d/rules: Skip tests requiring port 8888 to be available

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 27 Dec 2019 18:38:35 +0100

python-hbmqtt (0.9.5-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.
  * Bump Standards-Version to 4.4.1.

 -- Stein Magnus Jodal <jodal@debian.org>  Wed, 25 Dec 2019 08:59:41 +0100

python-hbmqtt (0.9.5-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Convert git repository from git-dpm to gbp layout

  [ Stein Magnus Jodal ]
  * New upstream release (Closes: #911861)
  * d/control: Bump Standards-Version to 4.4.0, no changes required
  * d/copyright: Update copyright years
  * d/patches: Move scripts module into hbmqtt module

 -- Stein Magnus Jodal <jodal@debian.org>  Thu, 18 Jul 2019 23:43:46 +0200

python-hbmqtt (0.9.2-2) unstable; urgency=medium

  * debian/control
    - Remove redundant X-Python3-Version
  * debian/patches/
    - Add patch to remove the `scripts` module (closes: 897691)

 -- Stein Magnus Jodal <jodal@debian.org>  Fri, 25 May 2018 11:30:53 +0200

python-hbmqtt (0.9.2-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org
  * d/copyright: Use https protocol in Format field

  [ Stein Magnus Jodal ]
  * New upstream release
  * debian/control
    - Bump Standards-Version to 4.1.4, no changes required
  * debian/patches/
    - Drop 0001-transitions-0.5.0-compatability.patch, as it is merged upstream
  * debian/rules
    - Enable tests, as upstream now includes the full suite
    - Install changelog, now included by upstream

 -- Stein Magnus Jodal <jodal@debian.org>  Wed, 02 May 2018 01:06:04 +0200

python-hbmqtt (0.9-1) unstable; urgency=medium

  * Initial release (closes: 869776)

 -- Stein Magnus Jodal <jodal@debian.org>  Wed, 26 Jul 2017 10:48:08 +0200
